package ua.nure.hpdp.lab1;

import java.awt.*;

public class ColorBuffer {
    public int r = 0;
    public int g = 0;
    public int b = 0;

    public ColorBuffer() {
    }

    public ColorBuffer(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    @Override
    public String toString() {
        return "Color{" +
                "r=" + r +
                ", g=" + g +
                ", b=" + b +
                '}';
    }

    public int toRGB() {
        return new Color(this.r, this.g, this.b).getRGB();
    }
}
