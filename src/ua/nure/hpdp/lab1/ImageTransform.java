package ua.nure.hpdp.lab1;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageTransform {

    public static int[][] transformMatrix = new int[][] {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}};
    private static final int normalizeCoefficient = 3;
    public void demo(String inputPath, String outputPath) throws IOException {
        File file = new File(inputPath);
        BufferedImage image = ImageIO.read(file);
        var timer = System.currentTimeMillis();
        BufferedImage output = this.process(image);
        System.out.println("Processing took " + (System.currentTimeMillis() - timer) + "ms");
        File outputFile = new File(outputPath);
        ImageIO.write(output, "jpg", outputFile);
    }

    public BufferedImage process(BufferedImage img) {
        BufferedImage out = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        for (int x = 1; x < img.getWidth(); x++) {
            for (int y = 1; y < img.getHeight(); y++) {
                var pixel = img.getRGB(x, y);
                var pixelColor = new Color(pixel);
                var newPixel = new ColorBuffer();
                for (int xm = x - 1; xm < x + 1; xm++) {
                    for (int ym = y - 1; ym < y + 1; ym++) {
                        newPixel.r += transformMatrix[xm - x + 1][ym - y + 1] * pixelColor.getRed();
                        newPixel.g += transformMatrix[xm - x + 1][ym - y + 1] * pixelColor.getGreen();
                        newPixel.b += transformMatrix[xm - x + 1][ym - y + 1] * pixelColor.getBlue();
                    }
                }
                newPixel.r /= normalizeCoefficient;
                newPixel.g /= normalizeCoefficient;
                newPixel.b /= normalizeCoefficient;
                out.setRGB(x, y, newPixel.toRGB());
            }
        }
        return out;
    }

}
