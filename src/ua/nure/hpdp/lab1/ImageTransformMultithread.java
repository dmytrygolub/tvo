package ua.nure.hpdp.lab1;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ImageTransformMultithread {
    public static int[][] transformMatrix = new int[][] {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}};
    private static final int normalizeCoefficient = 3;
    private final int numThreads;

    public ImageTransformMultithread(int numThreads) {
        this.numThreads = numThreads;
    }

    public void demo(String inputPath, String outputPath) throws IOException, InterruptedException {
        File file = new File(inputPath);
        BufferedImage image = ImageIO.read(file);
        var timer = System.currentTimeMillis();
        BufferedImage output = this.process(image);
        System.out.println("Processing took " + (System.currentTimeMillis() - timer) + "ms");
        File outputFile = new File(outputPath);
        ImageIO.write(output, "jpg", outputFile);
    }

    public BufferedImage process(BufferedImage img) throws InterruptedException {
        int width = img.getWidth();
        final int height = img.getHeight();
        BufferedImage out = new BufferedImage(width, height, img.getType());

        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);
        int lastX = 1;
        int windowSize = width / numThreads;
        int lastThread = 0;
        for (int i = 0; i < numThreads; i++) {
            final int startX = Integer.valueOf(lastX);
            int endX;
            if (lastThread < numThreads) {
                endX = lastX + windowSize;
            } else {
                endX = width;
            }
            executorService.submit(() -> {
                for (int x = startX; x < endX; x++) {
                    for (int y = 1; y < height; y++) {
                        var pixel = img.getRGB(x, y);
                        var pixelColor = new Color(pixel);
                        var newPixel = new ColorBuffer();
                        for (int xm = x - 1; xm < x + 1; xm++) {
                            for (int ym = y - 1; ym < y + 1; ym++) {
                                newPixel.r += transformMatrix[xm - x + 1][ym - y + 1] * pixelColor.getRed();
                                newPixel.g += transformMatrix[xm - x + 1][ym - y + 1] * pixelColor.getGreen();
                                newPixel.b += transformMatrix[xm - x + 1][ym - y + 1] * pixelColor.getBlue();
                            }
                        }
                        newPixel.r /= normalizeCoefficient;
                        newPixel.g /= normalizeCoefficient;
                        newPixel.b /= normalizeCoefficient;
                        out.setRGB(x, y, newPixel.toRGB());
                    }
                }
            });
            lastThread++;
            lastX = endX;
        }

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);

        return out;
    }
}
